DESCRIPTION = "minimalistic image"

IMAGE_FEATURES = " allow-empty-password ssh-server-openssh"

include core-image-minimal.bb

IMAGE_INSTALL = "\
	base-files \	
	busybox \
	initscripts \
	 openssh \
     openssh-sftp \
     openssh-sftp-server \
"


inherit core-image extrausers

#USE_DEVFS ?= "0"
IMAGE_ROOTFS_SIZE ?= "8192"
#EXTRA_USERS_PARAMS = "usermod -P root root;"

hostname_pn-base-files = "cdac"


#export MYIMAGE :="demo-cdac-ooo-cli"

PREFERRED_PROVIDER_virtual/bootloader_cdac-ooo ="u-boot-cdac"

SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "Recipe created manually"
LICENSE = "CLOSED"
#LIC_FILES_CHKSUM = "file://COPYING.MIT;md5=5750f3aa4ea2b00c2bf21b2b2a7b714d"


PR = "r0"

SRC_URI = "file://helloworld.c"

S = "${WORKDIR}"

INSANE_SKIP_${PN} += "ldflags"

do_compile(){

        ${CC} ${LD_FLAGS} helloworld.c -o helloworld

}

do_install() {

        install -d ${D}${bindir}
        install -m -777 helloworld ${D}${bindir}
}


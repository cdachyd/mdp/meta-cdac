SUMMARY = "Integration of a simle hello module"
LICENSE = "CLOSED"
inherit module
SRC_URI = "file://module-hello.c \
	   file://Makefile \
	"
S = "${WORKDIR}"

RPROVIDES_${PN} += "kernel-module-hellomod"

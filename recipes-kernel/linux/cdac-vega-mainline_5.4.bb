require recipes-kernel/linux/cdac-vega-mainline-common.inc

LINUX_VERSION = "v5.4.0"
KERNEL_VERSION_SANITY_SKIP="1"

BRANCH = "linux-5.4.y"
#SRCREV = "${AUTOREV}"
SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git;name=kernel;branch=${BRANCH} \
		file://0001-Vega-Full-WhiteFix-Test-17Nov.patch \
		file://0002-Updated-drivers-SPI-GPIO-I2C-and-Kconfigs.patch \
		file://0003-remove-hardcoded-cmdline.patch \
	"

# SRCREV_kernel = "52f6ded2a377ac4f191c84182488e454b1386239" for 5.4.72

SRCREV_kernel = "79438f37a69a80822333c86acb06a71abce1bce3"

PV = "${LINUX_VERSION}+git${SRCPV}"

#KBUILD_DEFCONFIG_cdac-main ="defconfig"

COMPATIBLE_MACHINE = "cdac-ooo|cdac-vega"

SRC_URI += "file://cdac-vega-defconfig"

#PATCHTOOL = "git"

#S = "${WORKDIR}/5.4.x+gitAUTOINC+bdc3a8f6a8-r0"
#file://Eth-and-INTcontroller.patch

